<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function skills()
    {
        return view('skills');
    }

    public function experience()
    {
        return view('experience');
    }

    public function contact()
    {
        return view('contact');
    }
}
