<div class="container-fluid fixed-top bg-dark">
    <div class="row justify-content-center">
        <div class="col-12 col-md-10">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <div class="container-fluid">
                  <a class="navbar-brand" href="{{route('welcome')}}"><img style="height: 45px" src="/img/favicon.png" alt=""></a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                      <li class="nav-item m-3">
                        <a class="nav-link text-center" href="{{route('welcome')}}">HOME</a>
                      </li>
                      <li class="nav-item m-3">
                        <a class="nav-link text-center" href="{{route('skills')}}">SKILLS</a>
                      </li>
                      <li class="nav-item m-3">
                        <a class="nav-link text-center" href="{{route('experience')}}">ESPERIENZE</a>
                      </li>
                      {{-- <li class="nav-item m-3">
                        <a class="nav-link text-center" href="#">Portfolio</a>
                      </li> --}}
                      <li class="nav-item m-3">
                        <a class="nav-link text-center" href="{{route('contact')}}">CONTATTAMI</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>

        </div>

    </div>
</div>
