<footer class="mt-5 n-bg-footer">
    <div class="container-fluid pb-5">
        <div class="row justify-content-center">
            <div class="col-9 col-md-4 mt-3 mb-4">
                <img class="img-fluid" src="./img/logo_large.png" alt="Logo Nicola Vitrani">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <p class="fw-bold text-center text-white">Contatti:</p>
                <p class="text-center"><a class="n-mail" href="tel:3924163838"><i class="fab fa-whatsapp"></i> 3924163838</a></p>
                <p class="text-center"><a class="n-mail" href="mailto:nicolavitrani93@gmail.com"><i class="far fa-envelope"></i> nicolavitrani93@gmail.com</a></p>
                <p class="text-center"><a class="n-mail" href="{{route('contact')}}"><i class="fas fa-feather-alt"></i> Form contatti</a></p>
            </div>
            <div class="col-12 col-md-4">
                <p class="fw-bold text-center text-white">La mia repository Gitlab:</p>
                <p class="text-center"><a class="n-mail" href="https://gitlab.com/nicolavitrani"><i class="fab fa-gitlab"></i> Gitlab</a></p>
            </div>
            <div class="col-12 col-md-4">
                <p class="fw-bold text-center text-white">I miei social:</p>
                <p class="text-center"><a class="n-mail" href="https://www.facebook.com/nicola.vitrani/"><i class="fab fa-facebook-square"></i> Facebook</a></p>
                <p class="text-center"><a class="n-mail" href="https://www.instagram.com/nicolavitrani/"><i class="fab fa-instagram"></i> Instagram</a></p>
                <p class="text-center"><a class="n-mail" href="https://www.linkedin.com/in/nicola-vitrani-205a2881/"><i class="fab fa-linkedin"></i> Linkedin</a></p>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12">
                <p class="text-white text-center">Developed by me</p>
            </div>
        </div>
    </div>
</footer>
