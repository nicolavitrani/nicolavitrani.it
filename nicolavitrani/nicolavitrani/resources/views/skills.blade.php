<x-layout>


    <div class="container-fluid nav-space">
        <div class="row nav-space">
            <div class="col-12">
                <h2>.</h2>
            </div>
        </div>
    </div>

    <div class="container mt-5 mb-4">
        <div class="row justify-content-center">
            <div class="col-12 mt-3">
                <h2 class="n-h1 text-center" id="skills">Skills</h2>
            </div>
            <div class="col-12 mb-2">
                <p class="text-center">LE MIE CONOSCENZE SUI SOFTWARE E SUI LINGUAGGI</p>
            </div>

            <div class="row mt-5 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">Adobe Photoshop & Illustrator</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">75%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">Git</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">50%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">HTML</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">85%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">CSS</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">80%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">Bootstrap</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">90%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">Javascript</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 45%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">45%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">PHP</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 65%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">65%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">Laravel</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 65%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">65%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">MySQL</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">50%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">O.O.P.</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">60%</div>
                    </div>
                </div>
            </div>

            <div class="row mt-3 align-items-center">
                <div class="col-3 align-items-center">
                    <p class="mb-0">Web Server Nginx</p>
                </div>

                <div class="col-8">
                    <div class="progress m-3">
                       <div class="progress-bar" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">70%</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</x-layout>
