<x-layout>

    <div class="container-fluid nav-space">
        <div class="row nav-space">
            <div class="col-12">
                <h2>.</h2>
            </div>
        </div>
    </div>

    <section>
        <div class="container mt-5 mb-4">
            <div class="row">
                <div class="col-12 mt-3">
                    <h1 class="n-h1 text-center">Contattami</h1>
                </div>
                <div class="col-12">
                    <p class="text-center">COMPILA IL FORM, TI RISPONDERO' AL PIÙ PRESTO</p>
                </div>
            </div>
        </div>

        {{-- form di bootstrap --}}

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-9 mt-5">
                    <form>
                        @csrf

                        <div class="mb-3">
                          <label class="form-label">Nome</label>
                          <input type="text" class="form-control" placeholder="Il tuo nome">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Cognome</label>
                            <input type="text" class="form-control" placeholder="Il tuo cognome">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Indirizzo e-mail</label>
                            <input type="email" class="form-control" placeholder="Inserisci la tua e-mail">
                        </div>
                            <div class="mb-3">
                                <label class="form-label">Messaggio</label>
                                <br>
                                <textarea rows="10" style="min-width: 100%" placeholder="Lasciami un messaggio"></textarea>
                            </div>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-12 mt-3">
                                    <p class="text-center"><button type="submit" class="p-2 n-btn n-button n-mail n-shadow-btn text-white">INVIA</button></p>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </section>

</x-layout>
