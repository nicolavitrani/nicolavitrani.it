<x-layout>

    <section>
        <div class="container-fluid n-masthead mb-2">
            <div class="row align-items-center justify-content-around py-3" style="min-height: 80vh"">
                <div class="col-12 col-md-4 mt-5">
                    <h1 class="n-h1 text-center">Ciao, sono</h1>
                    <h2 class="n-h1 text-center color-primary"><strong>Nicola Vitrani</strong></h2>
                    <p class="text-center mt-4">FULL-STACK DEVELOPER</p>
                </div>
                <div class="col-12 col-md-7 mt-4">
                    <img src="/img/webdeveloper.png" alt="Nicola Vitrani" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container mt-5 mb-3">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center n-h1">
                        Perché scegliere me?
                    </h2>
                </div>
            </div>
        </div>
        <div class="container mb-5">
            <div class="row justify-content-around align-items-center">
                <div class="col-11 col-md-4 mt-2">
                    <h3 class="m-3 text-center">COME LAVORO</h3>
                    <ol class="mt-3 mb-3">
                        <li class="mt-2">Lavoro in team e metodologia Agile</li>
                        <li class="mt-2">Puntualità ed affidabilità</li>
                        <li class="mt-2">Passione per la programmazione</li>
                        <li class="mt-2">Siti responsive</li>
                    </ol>
                </div>
                <div class="col-11 col-md-7 mt-2">
                    <img src="/img/To-do list.png" alt="to do list" class="img-fluid">
                </div>
            </div>
        </div>

        <div class="container mt-5">
            <div class="row justify-content-around align-items-center">
                <div class="col-12 col-md-6 order-1 order-md-2">
                    <h3 class="m-3 text-center">IL MIO CURRICULUM</h3>
                    <p class="text-center mt-5"><a class="n-mail" href="./files/CV Nicola Vitrani.pdf"><i class="far fa-file-pdf fs-1"></i></a></p>
                </div>
                <div class="col-9 col-md-5 order-2 order-md-1 mt-4">
                    <img src="/img/cv.png" alt="Curriculum" class="img-fluid">
                </div>
            </div>
        </div>

        <div class="container mt-5 mb-5">
            <div class="row mx-auto mt-5">
                <div class="col-12 mt-2">
                    <h3 class="m-3 text-center">
                        I MIEI LAVORI
                    </h3>
                </div>
            </div>
        </div>
        <div class="container mb-3">
            <div class="row">
                <div class="col-12 col-md-6">
                    <p class="text-center">
                        Ho realizzato un clone del famoso sito di annunci "subito" con l'implementazione delle funzioni principali:
                    </p>
                    <div class="container">
                        <div class="row mx-auto">
                            <div class="col-11">
                                <ul class="mt-2">
                                    <li>
                                        Autenticazione e creazione pannello "Revisore"
                                    </li>
                                    <li>
                                        Multilingua (Italiano, Inglese e Spagnolo)
                                    </li>
                                    <li>
                                        Ricerca full-text
                                    </li>
                                    <li>
                                        Utilizzo delle Google API
                                    </li>
                                    <li>
                                        Caricamento delle immagini con "Dropzone"
                                    </li>
                                    <li>
                                        Applicazione automatica del Watermark sulle immagini caricate
                                    </li>
                                    <li>
                                        Oscuramento automatico dei volti nelle immagini
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 my-3">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button n-bg-black text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Posso vedere il sito?
                            </button>
                          </h2>
                          <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body n-bg-black">
                              <p class="text-white">Certo, puoi visitare il mio profilo pubblico di <a class="n-mail" href="https://gitlab.com/nicolavitrani">Gitlab</a> per vedere il codice o puoi contattarmi per organizzare una demo: <a class="n-mail" href="{{route('contact')}}">Compila il form</a>.</p>
                            </div>
                          </div>
                        </div>
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed  n-bg-black text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Cosa hai utilizzato per realizzare questo progetto?
                            </button>
                          </h2>
                          <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body n-bg-black">
                              <p class="text-white">Ho utilizzato il framework di <strong>Laravel 8</strong> per la logica dinamica, <strong>Bootstrap 5</strong> e <strong>Javascript</strong> per il frontend e <strong>MySQL</strong> per la gestione del database.</p>
                            </div>
                          </div>
                        </div>
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed  n-bg-black text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Sapresti implementare un pannello di controllo per il proprietario del sito?
                            </button>
                          </h2>
                          <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body n-bg-black">
                              <p class="text white">
                                Certo, utilizzando <strong>Laravel Nova</strong> potrei permettere al proprietario del sito di apportare modifiche direttamente sul database della piattaforma.
                              </p>
                            </div>
                          </div>
                        </div>

                      </div>
                </div>
            </div>
        </div>

            <div class="container mt-5 mb-5">
                <div class="row mx-auto my-3">
                    <div class="col-12 justify-content-center">
                        <p class="text-center"><a class="n-mail n-btn n-shadow-btn p-3 text-white" href="{{route('contact')}}">CONTATTAMI</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- commento di prova --}}

</x-layout>
