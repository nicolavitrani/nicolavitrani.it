<x-layout>

    <div class="container-fluid nav-space">
        <div class="row nav-space">
            <div class="col-12">
                <h2>.</h2>
            </div>
        </div>
    </div>

    <div class="container mt-5 mb-4">
        <div class="row">
            <div class="col-12 mt-3">
                <h1 class="n-h1 text-center">Esperienza</h1>
            </div>
            <div class="col-12">
                <p class="text-center">ESPERIENZE LAVORATIVE E DI VOLONTARIATO</p>
            </div>
        </div>
    </div>

    {{-- Timeline --}}

    <div class="container">
        <div class="row justify-content-center mb-5 mt-5">
            <div class="col-1 mt-5">
                <p>
                    <i class="fas fa-angle-double-right"></i>
                </p>
            </div>
            <div class="col-9 col-md-4">
                <div class="">
                  <div class="content mt-5">
                    <h2 class="color-primary">dal 2020</h2>
                    <h4>Progetto Engage</h4>
                    <p>Gestione newsletter, eventi social network, grafica con Canva e Photoshop.</p>
                    <a class="n-mail" href="https://www.progettoengage.it">Visita il sito</a>
                  </div>
                </div>
            </div>
            <div class="col-7 col-md-3 mt-5 justify-content-center">
                <img src="./img/progetto.engage.logo.png" alt="Progetto Engage Logo" class="img-fluid mx-auto">
            </div>
            <div class="col-1 mt-5">
                <p class="text-end">
                    <i class="fas fa-angle-double-left"></i>
                </p>
            </div>
        </div>
        <div class="row justify-content-center mb-5 mt-5">
            <div class="col-1 mt-5">
                <p>
                    <i class="fas fa-angle-double-right"></i>
                </p>
            </div>
            <div class="col-9 col-md-4">
                <div class="">
                  <div class="content mt-5">
                    <h2 class="color-primary">2019</h2>
                    <h4>Agorà face2face</h4>
                    <p>Pianificazione di raccolta fondi per organizzazioni no-profit</p>
                    <a class="n-mail" href="https://www.agoraf2f.com/">Visita il sito</a>
                  </div>
                </div>
            </div>
            <div class="col-7 col-md-3 mt-5 justify-content-center">
                <img src="./img/agora.logo.png" alt="Progetto Engage Logo" class="img-fluid mx-auto">
            </div>
            <div class="col-1 mt-5">
                <p class="text-end">
                    <i class="fas fa-angle-double-left"></i>
                </p>
            </div>
        </div>
        <div class="row justify-content-center mb-5 mt-5">
            <div class="col-1 mt-5">
                <p>
                    <i class="fas fa-angle-double-right"></i>
                </p>
            </div>
            <div class="col-9 col-md-4">
                <div class="">
                  <div class="content mt-5">
                    <h2 class="color-primary">dal 2018</h2>
                    <h4>Banco Alimentare</h4>
                    <p>Grafiche per i social, newsletter, illustrazioni digitali. Volontario della Colletta Alimentare.</p>
                    <a class="n-mail" href="https://www.bancoalimentare.it/it/daunia/">Visita il sito</a>
                  </div>
                </div>
            </div>
            <div class="col-7 col-md-3 mt-5 justify-content-center">
                <img src="./img/banco.logo.jpg" alt="Progetto Engage Logo" class="img-fluid mx-auto">
            </div>
            <div class="col-1 mt-5">
                <p class="text-end">
                    <i class="fas fa-angle-double-left"></i>
                </p>
            </div>
        </div>
        <div class="row justify-content-center mb-5 mt-5">
            <div class="col-1 mt-5 mb-5">
                <p>
                    <i class="fas fa-angle-double-right"></i>
                </p>
            </div>
            <div class="col-9 col-md-4">
                <div class="">
                  <div class="content mt-5">
                    <h2 class="color-primary">2017</h2>
                    <h4>Poste Italiane</h4>
                    <p>Assunto nel ruolo di portalettere presso la proivncia di Bologna</p>
                    <a class="n-mail" href="https://www.poste.it/">Visita il sito</a>
                  </div>
                </div>
            </div>
            <div class="col-7 col-md-3 mt-5 justify-content-center">
                <img src="./img/poste.logo.png" alt="Progetto Engage Logo" class="img-fluid mx-auto">
            </div>
            <div class="col-1 mt-5">
                <p class="text-end">
                    <i class="fas fa-angle-double-left"></i>
                </p>
            </div>
        </div>
    </div>
</x-layout>

